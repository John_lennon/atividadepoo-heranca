/**
 * Created by tavz-correia on 12/5/16.
 */
public class Empregado extends Pessoa {
    private int codigoSetor;
    private double salarioBase;
    private double imposto;

    public Empregado(String nome, String endereco, String telefone){
        super(nome, endereco, telefone);
    }


    public double calcularSalario(){
        double salario = salarioBase - (salarioBase * imposto);
        return salario;
    }

    public void setCodigoSetor(int codigoSetor){
        this.codigoSetor = codigoSetor;
    }

    public int getCodigoSetor(){
        return codigoSetor;
    }

    public void setSalarioBase(double salarioBase){
        this.salarioBase = salarioBase;
    }

    public double getSalarioBase(){
        return salarioBase;
    }

    public void setImposto(double imposto){
        this.imposto = imposto;
    }

    public double getImposto(){
        return imposto;
    }

    @Override
    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                "Endereco: " + getEndereco() + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Codigo do setor: " + getCodigoSetor() + "\n" +
                "Salario Base: " + getSalarioBase() + "\n" +
                "Imposto: " + getImposto());
    }
}
