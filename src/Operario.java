/**
 * Created by tavz-correia on 12/5/16.
 */
public class Operario extends Empregado {
    private double valorProducao;
    private double comissao;

    public Operario(String nome, String endereco, String telefone){
        super(nome, endereco, telefone);
    }

    public void setValorProduçao(double valorProducao){
        this.valorProducao = valorProducao;
    }

    public double getValorProducao(){
        return valorProducao;
    }

    public double getComissao() {
        return comissao;
    }

    public void setComissao(double comissao) {
        this.comissao = comissao;
    }

    @Override
    public double calcularSalario(){
        double comissao = valorProducao / 100;
        setComissao(comissao);
        double salario = getSalarioBase() - (getSalarioBase() * getImposto()) + comissao;
        return salario;
    }

    @Override
    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                "Endereco: " + getEndereco() + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Codigo do setor: " + getCodigoSetor() + "\n" +
                "Salario Base: " + getSalarioBase() + "\n" +
                "Imposto: " + getImposto() + "\n" +
                "Valor Procucao: " + getValorProducao() + "\n" +
                "Comissao: " + getComissao());
    }
}
