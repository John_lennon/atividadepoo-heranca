/**
 * Created by tavz-correia on 12/5/16.
 */
public class Administrador extends Empregado{
    private double ajudaDeCusto;

    public Administrador(String nome, String endereco, String telefone){
        super(nome, endereco, telefone);
    }

    public void setAjudaDeCusto(double ajudaDeCusto){
        this.ajudaDeCusto = ajudaDeCusto;
    }

    public  double getAjudaDeCusto(){
        return ajudaDeCusto;
    }

    public double calcularSalario(){
        double salario = (getSalarioBase() - (getSalarioBase() * getImposto())) + this.ajudaDeCusto;
        return salario;
    }

    @Override
    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                "Endereco: " + getEndereco() + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Codigo do setor: " + getCodigoSetor() + "\n" +
                "Salario Base: " + getSalarioBase() + "\n" +
                "Ajuda de custos: " + getAjudaDeCusto() + "\n" +
                "Imposto: " + getImposto());
    }
}
