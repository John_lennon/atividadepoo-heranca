/**
 * Created by OtavioCorreia on 12/05/2016
 * Created by JohnLennon 12/06/2016
 */
public class Pessoa {
    private String nome;
    private String endereco;
    private String telefone;

    public Pessoa(){

    }

    public Pessoa(String nome){
        this.nome = nome;
    }

    public Pessoa(String nome, String endereco, String telefone) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return nome;
    }

    public void setEndereco(String endereco){
        this.endereco = endereco;
    }

    public String getEndereco(){
        return endereco;
    }

    public void setTelefone(String telefone){
        this.telefone = telefone;
    }

    public String getTelefone(){
        return telefone;
    }

    public void imprimePessoa(){
        System.out.println("Nome: " + getNome() + "\n" +
                           "Endereco: " + getEndereco() + "\n" +
                           "Telefone: " + getTelefone() + "\n");
    }
}
